<?php

// +----------------------------------------------------------------------
// | WeChatDeveloper
// +----------------------------------------------------------------------
// | 版权所有 2014~2023 ThinkAdmin [ thinkadmin.top ]
// +----------------------------------------------------------------------
// | 官方网站: https://thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// | 免责声明 ( https://thinkadmin.top/disclaimer )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/WeChatDeveloper
// | github 代码仓库：https://github.com/zoujingli/WeChatDeveloper
// +----------------------------------------------------------------------

use Lib\Contracts\DataArray;
use Lib\Exceptions\InvalidInstanceException;

/**
 * 加载缓存器
 *
 * Class La
 * @library WeChatDeveloper
 * @author 宝龙<117384790@qq.com>
 * @date
 *
 * ----- 拉卡拉支付 -----
 *
 * @method \LaPay\Order LaPayOrder($options = []) static 【聚合收银台】
 *
 * ----- 拉卡拉 云mis -----
 * @method \LaMis\Trade LaMisTrade($options = []) static 【推单】 
 *
 */


class La
{
    /**
     * 定义当前版本
     * @var string
     */
    const VERSION = '1.0.1';

    /**
     * 静态配置
     * @var DataArray
     */
    private static $config;

    /**
     * 设置及获取参数
     * @param array $option
     * @return array
     */
    public static function config($option = null)
    {
        if (is_array($option)) {
            self::$config = new DataArray($option);
        }
        if (self::$config instanceof DataArray) {
            return self::$config->get();
        }
        return [];
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws InvalidInstanceException
     */
    public static function __callStatic($name, $arguments)
    {
        if (substr($name, 0, 5) === 'LaPay') {
            $class = 'LaPay\\' . substr($name, 5);
        }
        if (substr($name, 0, 5) === 'LaMis') {
            $class = 'LaMis\\' . substr($name, 5);
        }
        if (!empty($class) && class_exists($class)) {
            $option = array_shift($arguments);
             
            $config = is_array($option) ? $option : self::$config->get();
            return new $class($config);
        }
        throw new InvalidInstanceException("class {$class} not found");
    }

}
