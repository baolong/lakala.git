<?php



namespace Lib\Contracts;

/**
 * 错误消息处理
 * Class DataError
 * @package WeChat\Contracts
 */
class DataError
{
    /**
     * 接口代码错误
     * @var array
     */
    static $message = [
        1       => '进行中',
        2       => '成功',
        3       => '失败',
        4       => '结果未知',
        5       => '取消',
        6       => '关闭',
    ];

    /**
     * 异常代码解析描述
     * @param string $code
     * @return string
     */
    public static function toMessage($code)
    {
        return isset(self::$message[$code]) ? self::$message[$code] : $code;
    }

}