<?php


namespace Lib\Contracts;

use Lib\Exceptions\InvalidArgumentException;
use Lib\Exceptions\InvalidResponseException;

/**
 * 拉卡拉支付基础类
 * Class BasicPay
 * @package WeChat\Contracts
 */
abstract class BasicMis
{
    /**
     * 商户配置
     * @var DataArray
     */
    protected $config;

    protected $schema = 'LKLAPI-SHA256withRSA';

    protected $version = '01';

    /**
     * 当前请求数据
     * @var DataArray
     */
    protected $params;

    /**
     * 静态缓存
     * @var static
     */
    protected static $cache;


    /**
     * 正常请求网关
     * @var string
     */
    protected $gateway = 'https://cmisapi.lakala.com/lklcmis-service/';


    /**
     * Lakala constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {

        if (empty($options['app_id'])) {
            throw new InvalidArgumentException("Missing Config -- [app_id] AppID 错误");
        }
        if (empty($options['mch_id'])) {
            throw new InvalidArgumentException("Missing Config -- [mch_id] 商户编号 错误");
        }
        if (empty($options['serial_no'])) {
            throw new InvalidArgumentException("Missing Config -- [serial_no] 证书编号 错误");
        }
        if (empty($options['notify_url'])) {
            throw new InvalidArgumentException("Missing Config -- [notify_url] 回调地址 错误");
        }
//        if (empty($options['app_public_key'])) {
//            throw new InvalidArgumentException("Missing Config -- [app_public_path] 接入方唯一编号 错误");
//        }
        if (empty($options['app_private_key'])) {
            throw new InvalidArgumentException("Missing Config -- [app_private_path] 应用私钥 错误");
        }
        if (empty($options['platform_public_key'])) {
            throw new InvalidArgumentException("Missing Config -- [platform_public_path] 平台公钥 错误");
        }


        if (!empty($options['cache_path'])) {
            Tools::$cache_path = $options['cache_path'];
        }
        if (!empty($options['debug'])) {
            $this->gateway = 'https://test.wsmsd.cn/sit/lklcmis-service/';
        }
        $this->config = new DataArray($options);
    }

    /**
     * 返回错误信息
     * @param string $code
     * @return string
     */
    public static function error(string $code){
        return DataError::toMessage($code);
    }

    /**
     * 静态创建对象
     * @param array $config
     * @return static
     */
    public static function instance(array $config): BasicMis
    {
        $key = md5(get_called_class() . serialize($config));
        if (isset(self::$cache[$key])) return self::$cache[$key];
        return self::$cache[$key] = new static($config);
    }

    /**
     * 生成支付签名
     * @param array $signData 参与签名的数据
     * @return array
     */
    protected function getSign(array $signData):array
    {
        ksort($signData);
        [$nonceStr,$timestamp] = [$signData['nonce'],$signData['ts']];
        $content = $this->_arr2str($signData,'&');
        openssl_sign($content, $sign, $this->getAppPrivateKey(), OPENSSL_ALGO_SHA256);
        return  [base64_encode($sign),$nonceStr,$timestamp];
    }


    protected function getNotifySign($data): bool
    {
        $data['body']['is_exist'] = $data['body']['is_exist'] ? 'true' :  'false';
        $signData = array_merge($data['body'],$data['header']);
        ksort($signData);
        $content = $this->_arr2str($signData,'&');
        $flag =  openssl_verify($content, base64_decode($data['sign']), $this->getPlatformPublicKey(), OPENSSL_ALGO_SHA256);
        if($flag) {
            return true;
        }
        return false;
    }

    /**
     * @param array $data
     * @param $signature
     * @return string
     */
    protected function authorization(array $data,&$signature): string
    {
        $signData = array_merge($data['body'],$data['header']);
        [$signature,$nonceStr,$timestamp] = $this->getSign($signData);
        return $this->_arr2str([
            'app_id' => $this->config->get('app_id'),
            'serial_no' => $this->config->get('serial_no'),
            'timestamp' => $timestamp,
            'nonce_str' => $nonceStr,
            'signature' => $signature,
        ]);
    }


    /**
     * 新版 数组转字符串
     * @param $array
     * @param string $charlist
     * @return string
     */
    private function _arr2str($array, string $charlist=','): string
    {
        $string = [];
        if ($array && is_array($array)) {
            foreach ($array as $key => $value) {
                $string[] = $key . '=' . ( is_array($value) ? json_encode($value,JSON_UNESCAPED_UNICODE) : $value );
            }
        }
        return join($charlist, $string);
    }

    /**
     * 获取应用私钥内容
     * @return string
     */
    private function getAppPrivateKey(): string
    {
        $content = wordwrap($this->trimCert($this->config->get('app_private_key')), 64, "\n", true);
        return "-----BEGIN PRIVATE KEY-----\n{$content}\n-----END PRIVATE KEY-----";
    }

    /**
     * 获取平台公钥内容
     * @return string
     */
    private function getPlatformPublicKey(): string
    {
        $content = wordwrap($this->trimCert($this->config->get('platform_public_key')), 64, "\n", true);
        return "-----BEGIN PUBLIC KEY-----\n{$content}\n-----END PUBLIC KEY-----";
    }

    /**
     * 去除证书前后内容及空白
     * @param string $sign
     * @return string
     */
    protected function trimCert($sign): string
    {
        return preg_replace(['/\s+/', '/-{5}.*?-{5}/'], '', $sign);
    }

    /**
     * 以 Post 请求接口
     * @param $url
     * @param $data
     * @param $needSignType
     * @param $type
     * @return mixed
     * @throws InvalidResponseException
     */
    protected function callPostApi($url, $data, $needSignType = true)
    {
        $options['headers'] = [ "Accept: application/json",  "Content-Type:application/json" ];
        [$nonceStr,$timestamp] = [Tools::createNoncestr(8),time()];        
        $postData = [ 'body' => $data, 'header' => [ 'ts' => time(),  'version' => $this->version ,'nonce' => $nonceStr,'app_id' =>  $this->config->get('app_id') ] ];
        if ($needSignType)  $options['headers'][] =  "Authorization: " . implode(' ',[$this->schema , $this->authorization($postData,$signature) ]);
        $postData['sign'] = $signature;
        $result = json_decode(Tools::post($this->gateway . $url ,json_encode($postData,JSON_UNESCAPED_UNICODE), $options),true);
        if (!isset($result['header']['code']) || $result['header']['code'] !== '00000') {
            throw new InvalidResponseException(
                "Error: " .
                (empty($result['header']['code']) ? '' : "{$result['header']['msg']} [{$result['header']['code']}]\r\n") .
                $result['header']['code'], $result
            );
        }
        return $result['body']??[];
    }
  
}
